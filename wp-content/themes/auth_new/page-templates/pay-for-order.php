<?php get_header(); ?>

<?php
/**
 * Template Name: Pay For Order
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
if (isset($_POST['order-id'])) {
	$order_id = $_POST['order-id'];
	
	if ($order = wc_get_order($order_id)) {
		$order_key = $order->order_key;

		$site_url = site_url();
		$redirect_link = "$site_url/checkout/order-pay/$order_id/?pay_for_order=true&key=$order_key";
		header("Location: $redirect_link ");
	}else{
	  echo '<script>alert("Please Enter A Correct Order ID");</script>';
  }
} ?>
<div class="pay-for-order-page-wrapper">
  
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-12 mx-auto">
        <div class="pay-for-order">
          <form action="" method="post">
            <input name="order-id" type="text" placeholder="Add Order Number">
            <button type="submit">SUBMIT</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<?php get_footer();