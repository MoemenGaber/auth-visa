<?php
/*
 * Template Name: Contact Us
 */
get_header();
?>
<style>
  .header{
    display: none;
  }
</style>

    <div class="contactUs">
        <div class="content">
            <div class="contact">
                <div class="googleMap">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d55342.86669818314!2d31.274070149999996!3d29.895141499999998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m3!3e6!4m0!4m0!5e0!3m2!1sen!2seg!4v1568233684529!5m2!1sen!2seg"  frameborder="0" style="border:0;" allowfullscreen="" ></iframe>
                </div>
                <div class="contactForm">
                    <h2>Contact Us <br><span>Keep in touch</span></h2>
                    <p><img src="<?php echo get_template_directory_uri(); ?>/assets/images/contact/location.png" alt=""> <?php echo get_field('address','option'); ?></p>
                    <p class="line-h"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/contact/phone.png" alt=""> <?php echo get_field('phone','option'); ?> <span class="have">Having difficulties contacting us?</span></p>
                    <p class="bottom"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/contact/call.png" alt=""> <?php echo get_field('phone_2','option'); ?></p>
                   <?php echo do_shortcode('[contact-form-7 id="328" title="Contact form 1"]'); ?>
                </div>
            </div>
        </div>

    </div>


<?php
get_footer();