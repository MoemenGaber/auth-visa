<?php
// Template Name: Homepage
get_header();
?>

<div class="home">
  <div class="content">
    <div class="container">
			<?php
			// check if the flexible content field has rows of data
			if (have_rows('home_page_builder')):
				// loop through the rows of data
				while (have_rows('home_page_builder')) : the_row(); ?>
					
					
					<?php if (get_row_layout() == 'wide_text_block'):
						$text_block = get_sub_field('text_block');
						$read_more_button = get_sub_field('read_more_button');
						?>
            <div class="us">
              <div class="firstBoxOfUs">
								<?php echo $text_block; ?>
								<?php if ($read_more_button) { ?>
                  <a href="<?php echo $read_more_button['url']; ?>">
                    <button class="read"><?php echo $read_more_button['title']; ?></button>
                  </a>
								<?php } ?>
              </div>
            </div>
					<?php endif; ?>
					
					
					<?php if (get_row_layout() == 'small_text_block'):
						$text_block = get_sub_field('text_block');
						?>
            <div class="country">
              <div class="description">
								<?php echo $text_block; ?>
              </div>
            </div>
					<?php endif; ?>
					
					
					<?php if (get_row_layout() == 'non_participating_countries'): ?>
            <div class="countries">
              <div class="countriesButtons">
								<?php
								$args = array('post_type'      => 'product',
								              'posts_per_page' => -1,
								              'product_cat'    => 'non-participating',
								              'orderby'        => 'rand');
								$loop = new WP_Query($args);
								while ($loop->have_posts()) : $loop->the_post();
									global $product; ?>
                  <a href="<?php echo get_permalink($loop->post->ID) ?>">
                    <button class="button-country"><?php echo $loop->post->post_title; ?></button>
                  </a>
								<?php endwhile; ?>
								<?php wp_reset_query(); ?>
              </div>
            </div>
					<?php endif; ?>
					
					
					<?php if (get_row_layout() == 'hague_convention_countries'): ?>
            <div class="countries">
              <div class="countriesButtons">
								<?php
								$args = array('post_type'      => 'product',
								              'posts_per_page' => -1,
								              'product_cat'    => 'hague-convention',
								              'orderby'        => 'rand');
								$loop = new WP_Query($args);
								while ($loop->have_posts()) : $loop->the_post();
									global $product; ?>
                  <a href="<?php echo get_permalink($loop->post->ID) ?>">
                    <button class="button-country"><?php echo $loop->post->post_title; ?></button>
                  </a>
								<?php endwhile; ?>
								<?php wp_reset_query(); ?>
              </div>
            </div>
					<?php endif; ?>
					
					<?php if (get_row_layout() == 'title_&_subtitle'):
						$the_title = get_sub_field('the_title');
						$the_sub_title = get_sub_field('the_sub_title');
						?>
            <div class="countries">
              <h2 class="countriesTitle countriesTitle2"><?php echo $the_title; ?>
								
								<?php if ($the_sub_title) { ?>
                  <span><?php echo $the_sub_title; ?></span>
								<?php } ?>
              </h2>
            </div>
					<?php endif; ?>
				
				
				<?php
				endwhile;
			endif;
			?>
    </div>
  </div>

</div>


<?php get_footer(); ?>
