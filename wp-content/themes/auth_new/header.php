<!doctype html>
<!--suppress CheckEmptyScriptTag -->
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<?php wp_head() ?>
<body class="page-<?php echo get_the_ID(); ?> <?php pagesClasses(); ?>">
<!-- region global-->
<div class="mainHeader">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item <?php if(is_front_page()){echo "active"; } ?>">
                        <a class="nav-link nav-links" href="<?php echo site_url(); ?>">Home</a>
                    </li>
                    <li class="nav-item <?php activeStatus('pay-invoice'); ?>">
                        <a class="nav-link nav-links" href="<?php echo get_permalink(get_page_by_path('pay-invoice')); ?>">PayInvoice</a>
                    </li>
                    <li class="nav-item <?php activeStatus('contact-us'); ?>">
                        <a class="nav-link nav-links" href="<?php echo get_permalink(get_page_by_path('contact-us')); ?>">Contact</a>
                    </li>
                    <li class="nav-item button">
                        <a class="nav-link nav-links" href="<?php echo wc_get_cart_url(); ?>"><i
                                    class="fas fa-shopping-cart"></i><span>(<?php echo WC()->cart->get_cart_contents_count(); ?>) Items</span></a>

                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>
<div class="header">
    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/globalHeader/background-header.png" alt="">
    <div class="container">
        <div class="content">
            <?php if(is_product()){ ?>
                <h2 class="headerTitle"><?php the_title(); ?></h2>
	        <?php }elseif(is_page()){
	        ?>
          <h2 class="headerTitle"><?php the_title(); ?></h2>
	
            <?php }else{
                ?>
                <h2 class="headerTitle">Certification <span>made easy</span></h2>

                <?php
            } ?>
        </div>
    </div>
</div>
<!-- endregion global-->
