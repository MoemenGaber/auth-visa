// jQuery(document).ready(function(){
//     jQuery('input[type="radio"]').click(function(){
//         if(jQuery(this).is(':checked')){
//             jQuery(this).attr("checked", false);
//         }
//         jQuery(this).attr("checked", true);
//     });
// });


jQuery(document).ready(function () {
    jQuery('.loading-overlay').hide();
    jQuery('.read-more').click(function () {
        jQuery('.readmore-content').slideToggle('fast');
    });
    jQuery('.woocommerce #payment #place_order').click(function () {
        jQuery('.loading-overlay').fadeIn();
    });
    
    
    jQuery('.qty').change(function () {
        const num = jQuery('.qty').val();
        const additional_value = window.fees.additional;
        const first_value = window.fees.first;
        if (num > 0) {
            let subTotal =0;
            let innerHTML = `${first_value?`<div>
                    <p>First Document Price</p>
                    <h3>$${window.fees.first} X <span class="docs_no">1</span> = <span>$${window.fees.first}</span></h3>
</div>`: ''}


${(num && additional_value)>1?`<div class="fee-group additional-price-div">
                         <p>Service fee for additional documents</p>
                         <h3>
                             $${window.fees.additional} X
                             <span class="additional_docs_no docs_no">${num-1}</span>
                              =
                             <span>$</span>
                             <span class="additional-price total_additional_doc_price_html">${window.fees.additional * (num- 1)}</span>
                         </h3>
                     </div>`:''}
                     ${window.fees.fees.length?window.fees.fees.map(fee=>{
                subTotal+=(fee.fee_price*num);
                return `<div class="fee-group">
                             <div>
                                 <p>${fee.fee_title}</p>
                                 <h3>$${fee.fee_price} X <span class="docs_no">${num}</span> = <span>$</span><span class="fee-price-${fee.fee_title}">${fee.fee_price*num}</span></h3>
                             </div>
                         </div>`;
            }).join(''):""}
                      <hr>
                     <div class="totalPrice">
                     <p class="">Total Amount</p>
                     <h3 class="price"><span>$</span><span id="grand-total">${+window.fees.first+window.fees.additional * (num- 1)+subTotal}</span></h3>
                     </div>
                    
                    `;
            jQuery('.freeForU').html(innerHTML);
    
            jQuery('.fees-block').slideDown('fast');
        }
        // console.log('#total_'+id);
    });
    
    
    jQuery('.letter').on('click', function () {
        let checkbox = jQuery(this).children('input[type="radio"]');
        checkbox.prop('checked', true);
        jQuery(this).addClass('active-choice').siblings().removeClass('active-choice');
        const id = jQuery(this).attr('id');
        let subChoices = jQuery(this).data('sub-choices');
        if (subChoices === 'with-sub-choices') {
            jQuery('#sub-and-readmore').show();
            jQuery('.letter-sub').hide(() => jQuery('.sub-' + id).show());
            jQuery('html, body').animate({
                scrollTop: jQuery('#sub-and-readmore').offset().top,
            }, 500);
        }
        if (subChoices === 'no-sub-choices') {
            jQuery('#sub-and-readmore').hide();
            jQuery('.letter-sub').hide();
            jQuery('.proceed-1').click();
        }
    });

// Next Buttons
    jQuery('.proceed-1').on('click', function (event) {
        event.preventDefault();
        
        if (!jQuery('input[name=\'choice\']:checked').val()) {
            alert('You must Choice one Option to continue');
        }
        else {
            if (jQuery('input[name=\'choice\']:checked').data('has-sub-choices') && jQuery('.letter-sub.active-choice').length === 0) {
                alert('You must Choice one sub-choice to continue');
            }
            else {
                
                jQuery('.step-1').hide();
                jQuery('.step-2').fadeIn(800);
                jQuery('.add-to-cart-div').fadeIn(800);
                
                let choice = jQuery('input[name=\'choice\']:checked');
                let subChoice = jQuery('.letter-sub.active-choice input[name="sub-choice-for-cart"]');
                jQuery('.choice-for-cart').attr('value', choice.attr('value'));
                jQuery('.choice-for-cart-id').attr('value', choice.data('choice-id'));
                jQuery('.sub-choice-for-cart').attr('value', subChoice.attr('value'));
                jQuery('.sub-choice-for-cart-id').attr('value', subChoice.data('sub-choice-id'));
    
    
                jQuery('.freeForU').html('');
                jQuery('.qty').val('0');
                jQuery(this).trigger('get-prices');
    
                jQuery(`h3.mainTitle[data-index="${choice.data('choice-id')}"]`).removeClass('d-none ');
            }
        }
    });
    
    jQuery('.proceed-3').on('click', function (event) {
        event.preventDefault();
        jQuery('.add-to-cart-div').hide();
        jQuery('.fees-block').hide();
        jQuery('.timeline').fadeIn(800);
        jQuery('#first_step').removeClass('active');
        jQuery('#second_step').addClass('active');
        jQuery('.step-2-content-steps').hide();
        
    });
    
    // Previous Button
    jQuery('.previous-2').on('click', function (event) {
        event.preventDefault();
        jQuery('.step-2').hide();
        jQuery('.step-1').fadeIn(800);
        
    });
    
    jQuery('.previous-1').on('click', function (event) {
        event.preventDefault();
        jQuery('.timeline').hide();
        jQuery('.add-to-cart-div').fadeIn(800);
        jQuery('.fees-block').fadeIn(800);
        
        jQuery('#first_step').addClass('active');
        jQuery('#second_step').removeClass('active');
        jQuery('.step-2-content-steps').fadeIn(800);
        
    });
    
    jQuery('.custom-qty').change(function () {
        let qty = jQuery('.custom-qty').val();
        jQuery('.custom_qty_cart').attr('value', qty);
    });
    
    
    // jQuery('.choseNumber').change(function(){
    //
    //     jQuery('button[name="update_cart"]').click();
    //
    //     jQuery('html, body').animate({
    //         scrollTop: jQuery(".documents").offset().top
    //     }, 2000);
    // });
    
    jQuery('.letter-sub').on('click', function () {
        jQuery('.proceed-1').click();
    });
});