jQuery(document).ready(function () {

    // Read more show / hide button
    jQuery('.read-more').click(function () {
        jQuery('.readmore-content').slideToggle("fast");
    });

    // Calculation Equation

    /* -------- Some Variables --------- */
    jQuery('input.fee-price').each(function () {
        let id = jQuery(this).attr('id');
        let val = jQuery(this).val();
        let additional_doc_num;
        let additional_docs_total;
        let additional_docs_price;
        additional_docs_price = jQuery('#additional_doc_price').attr('value');
        /* ----- END Variables ------ */
        jQuery('.qty').change(function () {
            // Some actions to be done on change Quantity dropdown
            jQuery('.fees-block').slideDown("fast");
            // Get quantity value
            let num = jQuery(".qty").val();
            jQuery('.qty_docs').attr('value', num);
            jQuery('.docs_no').html(num);
            jQuery('.timeline-docs-no').html(num);
            // Calculate Totals
            let total = val * num;
            additional_doc_num = num - 1;
            additional_docs_total = additional_docs_price * additional_doc_num;

            jQuery('.total_additional_doc_price_html').html(additional_docs_total);
            jQuery('#total_additional_doc_price').attr('value', additional_docs_total);
            jQuery('.additional_docs_no').html(additional_doc_num);
            jQuery('.' + id).html(total);
            jQuery('#total_' + id).attr('value', total);

            jQuery('.additional-price-div').toggle(num > 1);
        });

    });

    let final_pregrandtotal;
    let first_doc_fee;
    let total_additional_doc;
    let add_to_cart_final;
    let additional_docs_number;
    let add_to_cart_more_than_one;
    let additional_doc_fee;
    first_doc_fee = parseInt(jQuery('#first_document_price').attr('value'));
    console.log(first_doc_fee);
    additional_docs_number = parseInt(jQuery('.qty').attr('value'));
    additional_doc_fee = parseInt(jQuery('#additional_doc_price').attr('value'));


    jQuery('.qty').change(function () {
        let grandtotal = 0;
        let pre_grandtotal = 0;
        const id = jQuery('input.total').attr('id');
        jQuery('.total').each(function () {
            pre_grandtotal += parseFloat(jQuery(this).attr('value'));
        });
        console.log(pre_grandtotal);
        total_additional_doc = jQuery('#total_additional_doc_price').attr('value');
        final_pregrandtotal = pre_grandtotal - total_additional_doc;
        add_to_cart_final = (final_pregrandtotal + first_doc_fee);
        if (additional_docs_number > 1) {
            add_to_cart_more_than_one = add_to_cart_final - additional_doc_fee;
            console.log(add_to_cart_more_than_one);

            jQuery('.grand-total-to-cart').attr('value', add_to_cart_more_than_one);
            // Grand total for customer before cart
        }
        grandtotal = pre_grandtotal + first_doc_fee;
        jQuery('#grand-total').html(grandtotal);

        jQuery('input.grand-total').attr('value', grandtotal);
        const countryID = jQuery('input.country-id').attr('value');
        const qty = jQuery("#docs_number").val();
        // jQuery("form").attr("action", "/cart/?add-to-cart="+countryID+"&quantity="+num);


    });
    
    jQuery('input').on('click',function () {
        const id = jQuery(this).parent().attr('id');
        const subChoices = jQuery(this).parent().data('sub-choices');
        if (subChoices === 'with-sub-choices') {
            jQuery('.sub-' + id).slideDown();
        }
    });

// Next Buttons
    jQuery(".proceed-1").click(function (event) {
        event.preventDefault();
        if (!jQuery("input[name='choice']:checked").val()) {
            alert('You must Choice one Option to continue');
        }
        else {
            jQuery('.step-1').hide();
            jQuery('.step-2').show();
            jQuery('.add-to-cart-div').show();
            let choice = jQuery("input[name='choice']:checked").attr('value');
            jQuery('.choice-for-cart').attr('value', choice);
        }


    });
    jQuery(".proceed-3").click(function (event) {
        event.preventDefault();
        jQuery('.add-to-cart-div').hide();
        jQuery('.fees-block').hide();
        jQuery('.timeline').show();
        jQuery('#first_step').removeClass('active');
        jQuery('#second_step').addClass('active');
        jQuery('.step-2-content-steps').hide();
    });

    // Previous Button
    jQuery(".previous-2").click(function (event) {
        event.preventDefault();
        jQuery('.step-2').hide();
        jQuery('.step-1').show();

    });

    jQuery(".previous-1").click(function (event) {
        event.preventDefault();
        jQuery('.timeline').hide();
        jQuery('.add-to-cart-div').show();
        jQuery('.fees-block').show();
        jQuery('#first_step').addClass('active');
        jQuery('#second_step').removeClass('active');
        jQuery('.step-2-content-steps').show();

    });

    jQuery(".custom-qty").change(function () {
        let qty = jQuery('.custom-qty').val();
        jQuery('.custom_qty_cart').attr('value', qty)
    });
});