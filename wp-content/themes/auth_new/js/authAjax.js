jQuery(function ($) {
    $('select[name$="[qty]"]').on('change', function (event) {
        event.preventDefault();
        const $loading = $('.loading-overlay');
        const $this = $(this);
        $.ajax({
            url: auth_user_ajax_params.ajax_url, // AJAX handler
            data: {
                'action': 'authUserAjax',
                'ajax_type': auth_user_ajax_params.ajax_type['update_cart'],
                'cart_item_key': $this.data('cart-item-key'),
                'cart_item_quantity': $this.val(),
            },
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                $loading.fadeIn();
            },
            success: function (data) {
                if (data) {
                    $loading.fadeOut();
                    $('#total-amount').html(data.total);
                    if (data.product) {
                        $this.parent().siblings('.price').html(data.product);
                    }
                    else {
                        $this.closest('.cart_item').remove();
                    }
                }
            },
        });
    });
    $('.proceed-1').on('click', function (event) {
        event.preventDefault();
        const $loading = $('.loading-overlay');
        // const $this = $(this);
        $.ajax({
            url: auth_user_ajax_params.ajax_url, // AJAX handler
            data: {
                'action': 'authUserAjax',
                'ajax_type': auth_user_ajax_params.ajax_type['get_prices'],
                'choice_for_cart_id': $('.choice-for-cart-id').val(),
                'sub_choice_for_cart_id': $('.sub-choice-for-cart-id').val(),
                'product_id': $('.country-id').val(),
                // 'choice_for_cart': jQuery('.choice-for-cart'),
                // 'sub_choice_for_cart': jQuery('.sub-choice-for-cart'),
            },
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                $loading.fadeIn();
            },
            success: function (data) {
                $loading.fadeOut();
                if (data) {
                    window.fees = data;
                }
            },
        });
    });
    
});
