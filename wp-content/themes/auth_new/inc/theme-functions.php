<?php

####################################################
# Styles
####################################################
add_action('wp_enqueue_scripts', function () {
    //css
    wp_enqueue_style('all-css', 'https://use.fontawesome.com/releases/v5.8.1/css/all.css');
    wp_enqueue_style('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css');
    wp_enqueue_style('homepage', get_template_directory_uri() . '/assets/css/pages/home.css');
    wp_enqueue_style('wizard', get_template_directory_uri() . '/assets/css/pages/mainWizard.css');
    wp_enqueue_style('mo-css', get_template_directory_uri() . '/assets/css/mo_style.css');
    //js
    //    wp_enqueue_script('jQuery', 'https://code.jquery.com/jquery-3.4.1.min.js', '1.0', true);
    //    wp_enqueue_script('jQuery-slim', 'https://code.jquery.com/jquery-3.2.1.slim.min.js', '1.0', true);
    wp_enqueue_script('proper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', '1.0', true);
    wp_enqueue_script('Bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', '1.0', true);
    
    wp_enqueue_script('custom-scripts', get_template_directory_uri() . '/assets/js/custom_scripts.js', '1.0', true);
    
    wp_localize_script('main-js', 'os', ['ajax_url' => admin_url('admin-ajax.php')]);
});
####################################################
# ACF Options Page
####################################################
if (function_exists('acf_add_options_page')) {
    
    acf_add_options_page(array(
                             'page_title' => __('Theme General Settings'),
                             'menu_title' => __('Theme Options'),
                             'menu_slug'  => 'theme-general-settings',
                         ));
    
}


/* Functions for Woocommerce single product! */


// function prefix_add_discount_line($cart)
// {
//     foreach ($cart->get_cart() as $cart_item){
//         $grand_total = $cart_item['test_field'];
//     }
//     $cart->add_fee(__('Other Fees', 'yourtext-domain'), +$grand_total);
// }
// add_action('woocommerce_cart_calculate_fees', 'prefix_add_discount_line');


function countryTabs() {
    global $product;
    
    $timeline_day = get_field('second_step_timeline_day');
    $timeline_month = get_field('second_step_timeline_month');
    $timeline_day_number = get_field('second_step_timeline_date_number');
    
    ?>
  <div class="loading-overlay">
    <i class="fas fa-spinner fa-pulse"></i>
  </div>
  <div class="step-1">
    <div class="wizard02">
      <div class="content">
        <div class="container">
          <div class="documents">
            <div class="document">
              <div class="customProgress">
                <div class="prg prg1">
                  <img class="yes" src="<?php echo get_template_directory_uri(); ?>/assets/images/wizard02-1/yes.png" alt="">
                </div>
                <div class="prg prg2">
                  <img class="yes" src="<?php echo get_template_directory_uri(); ?>/assets/images/wizard02-1/yes.png" alt="">
                  <div class="background" id="numTwo"></div>
                </div>
                <div class="prg prg3">
                  <img class="yes" src="<?php echo get_template_directory_uri(); ?>/assets/images/wizard02-1/yes.png" alt="">
                  <div class="background" id="numThree"></div>
                </div>
                <div class="prg prg4">
                  <img class="yes" src="<?php echo get_template_directory_uri(); ?>/assets/images/wizard02-1/yes.png" alt="">
                  <div class="background" id="numFour"></div>
                </div>
              
              </div>
              <div class="numbers">
                <div class="number custom">
                  <h2 class="active"><span class="nub">01</span> <span><span class="step">Step 1</span><span
                        class="doc">Documents</span></span>
                  </h2>
                </div>
                <div class="number custom">
                  <h2 class="numTwo" data-width="#numTwo"><span class="nub">02</span> <span><span
                        class="step">Step 2</span><span class="doc">Timeline</span></span>
                  </h2>
                </div>
                <div class="number custom">
                  <h2 class="numThree" data-width="#numThree"><span class="nub">03</span> <span><span
                        class="step">Step 3</span><span
                        class="doc order">Order Details</span></span>
                  </h2>
                </div>
                <div class="number custom">
                  <h2 class="numFour" data-width="#numFour"><span class="nub">04</span> <span><span
                        class="step">Step 4</span><span class="doc">Payment</span></span>
                  </h2>
                </div>
              </div>
              <div class="chose">
                <h2 class="please">Please Choose <span class="ab">A, B, C <span class="or">or</span> D</span>
                </h2>
              </div>
              <div class="letters">
                  <?php
                  $choice_id = 0;
                  while (have_rows('first_step')) {
                      the_row();
                      while (have_rows('choice')) {
                          the_row();
                          ?>
                        <!-- Choice for Country -->
                        <div id="choice-<?php echo $choice_id; ?>" class="letter choice choice-<?php echo $choice_id; ?>"
                             data-sub-choices="<?php echo get_sub_field('are_there_sub-choices') ? "with-sub-choices" : "no-sub-choices" ?>">
                          <h3 class="choice-letter choice-<?php the_sub_field('choice_letter'); ?>"><?php the_sub_field('choice_letter'); ?></h3>
                          
                          <input class="choice-input" name="choice" type="radio"
                                 data-choice-id="<?=$choice_id++?>"
                                 data-has-sub-choices="<?php the_sub_field('are_there_sub-choices') ?>"
                                 value="<?php the_sub_field('choice_letter'); ?>-<?php the_sub_field('choice_title'); ?>"/>
                          <br>
                          <p><?php the_sub_field('choice_title'); ?></p>
                        </div>
                        <!-- END Choice for country -->
                          <?php
                      }
                  } ?>
              </div> <!-- End of Letters -->
              <div id="sub-and-readmore" class="documents document2" style="margin-top: 20px; display: none">
                <div class="document company">
                    <?php
                    while (have_rows('first_step')) {
                        the_row();
                        
                        $choice_id = 0;
                        while (have_rows('choice')) {
                            the_row();
                            $letter = get_sub_field('choice_letter');
                            ?>
                          <!-- START Sub-choices -->
                            <?php
                            if (get_sub_field('are_there_sub-choices')) {
                                if (have_rows('sub_choices')) {
                                    ?>
                                  
                                  <div class="letters">
                                      <?php
                                      $sub_choice_id = 0;
                                      while (have_rows('sub_choices')) {
                                          the_row();
                                          ?>
                                        <div class="letter letter-sub choice sub-choice-<?php echo $choice_id; ?>"
                                             style="display: none; margin-left: 19px">
                                          <h3 class="choice-letter"><?php echo $letter; ?></h3>
                                          <input name="sub-choice-for-cart" type="radio"
                                                 data-sub-choice-id="<?=$sub_choice_id++?>"
                                                 value="<?php the_sub_field('choice_title'); ?>"/>
                                          
                                          <br>
                                          <p><?php the_sub_field('choice_title'); ?></p>
                                        </div>
                                          
                                          <?php
                                          
                                      }
                                      ?>
                                  </div>
                                    <?php
                                }
                            }
                            $choice_id++;
                        }
                        
                    }
                    
                    ?>
                  <!-- END Sub choices -->
                </div>
              </div>
              
              <div class="nextPrev">
                
                <button class="next-btn proceed-1">Proceed</button><!-- END OF STEP 1 -->
              </div>
            </div> <!-- End of step 1 -->
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="step-2" style="display: none">
    <div class="wizard02">
      <div class="content">
        <div class="container">
          <div class="documents step-2-content-steps" style="display: block">
            <div class="document">
              <div class="customProgress">
                <div class="prg prg1">
                  <img class="yes" src="<?php echo get_template_directory_uri(); ?>/assets/images/wizard02-1/yes.png" alt="">
                </div>
                <div class="prg prg2">
                  <img class="yes" src="<?php echo get_template_directory_uri(); ?>/assets/images/wizard02-1/yes.png" alt="">
                  <div class="background" id="numTwo"></div>
                </div>
                <div class="prg prg3">
                  <img class="yes" src="<?php echo get_template_directory_uri(); ?>/assets/images/wizard02-1/yes.png" alt="">
                  <div class="background" id="numThree"></div>
                </div>
                <div class="prg prg4">
                  <img class="yes" src="<?php echo get_template_directory_uri(); ?>/assets/images/wizard02-1/yes.png" alt="">
                  <div class="background" id="numFour"></div>
                </div>
              
              </div>
              <div class="numbers">
                <div class="number custom">
                  <h2 class="active"><span class="nub">01</span> <span><span class="step">Step 1</span><span
                        class="doc">Documents</span></span>
                  </h2>
                </div>
                <div class="number custom">
                  <h2 class="numTwo" data-width="#numTwo"><span class="nub">02</span> <span><span
                        class="step">Step 2</span><span class="doc">Timeline</span></span>
                  </h2>
                </div>
                <div class="number custom">
                  <h2 class="numThree" data-width="#numThree"><span class="nub">03</span> <span><span
                        class="step">Step 3</span><span
                        class="doc order">Order Details</span></span>
                  </h2>
                </div>
                <div class="number custom">
                  <h2 class="numFour" data-width="#numFour"><span class="nub">04</span> <span><span
                        class="step">Step 4</span><span class="doc">Payment</span></span>
                  </h2>
                </div>
              </div>
              <div class="document2">
                <div class="company" id="documents-no">
                    <?php
                    $index = 0;
                    while (have_rows('first_step_choice')) {
                        the_row();
                        $read_more_button = get_sub_field('read_more_button');
                        $read_more_content = get_sub_field('read_more_content');
                        
                        if ($read_more_button) {
                            
                            ?>
                          <h3 class="d-none mainTitle" data-index="<?php echo $index; ?>">
                              <?php echo $read_more_content['read_more_text']; ?>
                          </h3>
                        <?php }
                        
                        $index++;
                        
                    } ?>
                  <div class="selections">
                    <div class="descriptionP">
                      <p class="relative"><span class="minus"></span>Certificate to Foreign Government (CFG).</p>
                      <p class="relative"><span class="minus"></span>Certificate of Pharmaceutical Products. (CPP)
                      </p>
                      <p class="relative"><span class="minus"></span>Certificate of Exportability.</p>
                      <p class="relative"><span class="minus"></span>Free Sale Certificate issued by the FDA </p>
                      <p class="relative"><span class="minus"></span>USDA Animal Health Certificate</p>
                      <p class="relative"><span class="minus"></span>USPTO Trademark Certificate</p>
                    </div>
                    <div class="descriptionP">
                      <p class="relative"><span class="minus"></span>Certificate of Environmental Protection
                        Agency</p>
                      <p class="relative"><span class="minus"></span>Export Certificate for Animal Products</p>
                      <p class="relative"><span class="minus"></span>Certified copy of Naturalization from
                        Homeland Security</p>
                      <p class="relative"><span class="minus"></span>Phytosanitary or Plant Certificate</p>
                      <p class="relative"><span class="minus"></span>FBI Background Check</p>
                      <p class="relative"><span class="minus"></span>USPTO Patent Certificate</p>
                    </div>
                  </div>
                  <form class="cart" action="<?php echo esc_url(apply_filters('woocommerce_add_to_cart_form_action', $product->get_permalink())); ?>" method="post"
                        enctype='multipart/form-data'>
                      <?php do_action('woocommerce_before_add_to_cart_button'); ?>
                    
                    <div class="add-to-cart-div">
                        <?php
                        do_action('woocommerce_before_add_to_cart_quantity');
                        
                        woocommerce_quantity_input(array(
                                                       'min_value'   => apply_filters('woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product),
                                                       'max_value'   => apply_filters('woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product),
                                                       'input_value' => isset($_POST['quantity']) ? wc_stock_amount(wp_unslash($_POST['quantity'])) : $product->get_min_purchase_quantity(),
                                                       // WPCS: CSRF ok, input var ok.
                                                   ));
                        
                        do_action('woocommerce_after_add_to_cart_quantity');
                        ?>
                    
                    </div>
                    
                    <div class="fees-block" style="display:none">
                      <div class="service">
                        <div class="freeForU"></div>
                      </div>
                    </div>
                    <div id="content2"></div>
                    <input type="text" value="<?php echo $product->get_id(); ?>" name="country-id" class="country-id" hidden>
                    <div class="nextPrev">
                      
                      <button class="next-btn previous-2">Back</button><!-- END OF STEP 1 -->
                      <button class="next-btn proceed-3">Proceed</button><!-- END OF STEP 1 -->
                    
                    </div>
                </div>
              </div>
            </div> <!-- END OF Step 2 -->
          
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="step-3 timeline" style="display: none">
    <div class="wizard-02">
      <div class="content">
        <div class="container">
          <div class="documents">
            <div class="document">
              <div class="customProgress">
                <div class="prg prg1">
                  <img class="yes" src="<?php echo get_template_directory_uri(); ?>/assets/images/wizard02-1/yes.png" alt="">
                </div>
                <div class="prg prg2">
                  <img class="yes" src="<?php echo get_template_directory_uri(); ?>/assets/images/wizard02-1/yes.png" alt="">
                  <div class="background" id="numTwo"></div>
                </div>
                <div class="prg prg3">
                  <img class="yes" src="<?php echo get_template_directory_uri(); ?>/assets/images/wizard02-1/yes.png" alt="">
                  <div class="background" id="numThree"></div>
                </div>
                <div class="prg prg4">
                  <img class="yes" src="<?php echo get_template_directory_uri(); ?>/assets/images/wizard02-1/yes.png" alt="">
                  <div class="background" id="numFour"></div>
                </div>
              
              </div>
              <div class="numbers">
                <div class="number custom">
                  <h2><span class="nub">01</span> <span><span class="step">Step 1</span><span
                        class="doc">Documents</span></span>
                  </h2>
                </div>
                <div class="number custom">
                  <h2 class="numTwo active" data-width="#numTwo"><span class="nub">02</span> <span><span
                        class="step">Step 2</span><span class="doc">Timeline</span></span>
                  </h2>
                </div>
                <div class="number custom">
                  <h2 class="numThree" data-width="#numThree"><span class="nub">03</span> <span><span
                        class="step">Step 3</span><span
                        class="doc order">Order Details</span></span>
                  </h2>
                </div>
                <div class="number custom">
                  <h2 class="numFour" data-width="#numFour"><span class="nub">04</span> <span><span
                        class="step">Step 4</span><span class="doc">Payment</span></span>
                  </h2>
                </div>
              </div>
              
              <div class="timeLine">
                <p>Timeline to legalize <span class="firstSpan timeline-docs-no"></span> Documents for use in
                  <span class="lastSpan"><?php echo $product->get_title(); ?></span></p>
              </div>
              <div class="date">
                <h3><?php echo $timeline_day; ?></h3>
                <h2><?php echo $timeline_day_number; ?></h2>
                <p><?php echo $timeline_month; ?></p>
              </div>
              <div class="selections">
                <div class="descriptionP">
                  <p class="relative"><span class="minus"></span>Customer is responsible for all shipping charges.</p>
                  <p class="relative"><span class="minus"></span>Please be sure to include a prepaid return shipping label or envelope.
                  </p>
                  <p class="relative"><span class="minus"></span>Certificate of Exportability.</p>
                  <p class="relative"><span class="minus"></span>This timeline is based upon using next day FedEx, UPS or USPS shipping. </p>
                  <p class="relative"><span class="minus"></span>The timeline above is adjustable to reflect your choice of carrier and number of days of shipping.</p>
                  <p class="relative"><span class="minus customMinus"></span>This is a good faith estimate based upon our recent experience with the embassy of egypt.</p>
                </div>
              </div>
              
              <input type="number" value="40" name="just_test_input" hidden>
              <div class="nextPrev">
                <button class="next-btn previous-1">Back</button><!-- END OF STEP 1 -->
                <button type="submit" name="add-to-cart" value="<?php echo esc_attr($product->get_id()); ?>"
                        class="single_add_to_cart_button button alt"><?php echo esc_html($product->single_add_to_cart_text()); ?></button>
              </div>
                <?php do_action('woocommerce_after_add_to_cart_button'); ?>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    
    <?php
}

add_action('woocommerce_before_single_product_summary', 'countryTabs');


//Store the custom field
add_filter('woocommerce_add_cart_item_data', 'add_cart_item_custom_data_vase', 10, 2);
function add_cart_item_custom_data_vase($cart_item_meta, $product_id) {
    global $woocommerce;
    $cart_item_meta['choice_for_cart'] = $_POST['choice-for-cart'];
    $cart_item_meta['choice_for_cart_id'] = $_POST['choice-for-cart-id'];
    $cart_item_meta['sub_choice_for_cart'] = $_POST['sub-choice-for-cart'];
    $cart_item_meta['sub_choice_for_cart_id'] = $_POST['sub-choice-for-cart-id'];
    $cart_item_meta['test_field'] = $_POST['grand-total-to-cart'];
    return $cart_item_meta;
}

//Get it from the session and add it to the cart variable
function get_cart_items_from_session($item, $values, $key) {
    if (array_key_exists('test_field', $values)) {
        $item['mmCentre'] = $values['test_field'];
    }
//
//    $item['choice_for_cart'] = $values['choice_for_cart'];
//    $item['choice_for_cart_id'] = $values['choice_for_cart_id'];
//    $item['sub_choice_for_cart'] = $values['sub_choice_for_cart'];
//    $item['sub_choice_for_cart_id'] = $values['sub_choice_for_cart_id'];
    return $item;
}

add_filter('woocommerce_get_cart_item_from_session', 'get_cart_items_from_session', 1, 3);

add_action('woocommerce_before_add_to_cart_button', 'custom_hidden_product_field', 11);
function custom_hidden_product_field() {
    echo '<input type="hidden" value="" class="grand-total-to-cart" name="grand-total-to-cart">';
    echo '<input type="hidden" value="This is test for value" class="choice-for-cart" name="choice-for-cart">';
    echo '<input type="hidden" value="This is test for value" class="choice-for-cart-id" name="choice-for-cart-id">';
    echo '<input type="hidden" value="null" class="sub-choice-for-cart" name="sub-choice-for-cart">';
    echo '<input type="hidden" value="null" class="sub-choice-for-cart-id" name="sub-choice-for-cart-id">';
}


/*
* Changing the minimum quantity to 2 for all the WooCommerce products
*/
function woocommerce_quantity_input_min_callback($min, $product) {
    $min = 1;
    return $min;
}

add_filter('woocommerce_quantity_input_min', 'woocommerce_quantity_input_min_callback', 10, 2);
/*
* Changing the maximum quantity to 5 for all the WooCommerce products
*/
function woocommerce_quantity_input_max_callback($max, $product) {
    $max = 7;
    return $max;
}

add_filter('woocommerce_quantity_input_max', 'woocommerce_quantity_input_max_callback', 10, 2);


/*
* Get the total quantity of the product available in the cart.
*/
function wc_qty_get_cart_qty($product_id, $cart_item_key = '') {
    global $woocommerce;
    $running_qty = 0; // iniializing quantity to 0
    // search the cart for the product in and calculate quantity.
    foreach ($woocommerce->cart->get_cart() as $other_cart_item_keys => $values) {
        if ($product_id == $values['product_id']) {
            if ($cart_item_key == $other_cart_item_keys) {
                continue;
            }
            $running_qty += (int)$values['quantity'];
        }
    }
    return $running_qty;
}

/*
* Validate product quantity when cart is UPDATED
*/
function wc_qty_update_cart_validation($passed, $cart_item_key, $values, $quantity) {
    $product_min = 1;
    $product_max = 7;
    if (!empty($product_min)) {
        // min is empty
        if (false !== $product_min) {
            $new_min = $product_min;
        } else {
            // neither max is set, so get out
            return $passed;
        }
    }
    if (!empty($product_max)) {
        // min is empty
        if (false !== $product_max) {
            $new_max = $product_max;
        } else {
            // neither max is set, so get out
            return $passed;
        }
    }
    $product = wc_get_product($values['product_id']);
    $already_in_cart = wc_qty_get_cart_qty($values['product_id'], $cart_item_key);
    if (($already_in_cart + $quantity) > $new_max) {
        wc_add_notice(apply_filters('wc_qty_error_message', sprintf(__('You can add a maximum of %1$s %2$s\'s to %3$s.', 'woocommerce-max-quantity'), $new_max, $product->get_name(),
                                                                    '<a href="' . esc_url(wc_get_cart_url()) . '">' . __('your cart', 'woocommerce-max-quantity') . '</a>'), $new_max), 'error');
        $passed = false;
    }
    if (($already_in_cart + $quantity) < $new_min) {
        wc_add_notice(apply_filters('wc_qty_error_message', sprintf(__('You should have minimum of %1$s %2$s\'s to %3$s.', 'woocommerce-max-quantity'), $new_min, $product->get_name(),
                                                                    '<a href="' . esc_url(wc_get_cart_url()) . '">' . __('your cart', 'woocommerce-max-quantity') . '</a>'), $new_min), 'error');
        $passed = false;
    }
    return $passed;
}

add_filter('woocommerce_update_cart_validation', 'wc_qty_update_cart_validation', 1, 4);

function woocommerce_quantity_input($args = array(), $product = null, $echo = true) {
    
    if (is_null($product)) {
        $product = $GLOBALS['product'];
    }
    ?>
  <h3 class="title">Please choose the total number of your documents</h3>
  <div class="quantity1">
    <label class="icon" for="chose">
      
      <select name="quantity" class="choseNumber input-text qty text">
        <option value="0">---</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
      
      </select>
    </label>
  </div>
    <?php
}

add_filter('add_to_cart_text', 'woo_custom_single_add_to_cart_text');                // < 2.1
add_filter('woocommerce_product_single_add_to_cart_text', 'woo_custom_single_add_to_cart_text');  // 2.1 +

function woo_custom_single_add_to_cart_text() {
    
    return __('Proceed', 'woocommerce');
    
}

add_action('woocommerce_before_calculate_totals', 'add_custom_price');

function add_custom_price($cart_object) {
    
    if ((is_admin() && !defined('DOING_AJAX'))) {
        return;
    }
    
    if (did_action('woocommerce_before_calculate_totals') >= 2) {
        return;
    }
    
    foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
        update_item_price($cart_item);
        //    echo '<pre>';var_dump($cart_item['data']);echo '</pre>';
    }
    
}

function iconic_add_engraving_text_to_order_items($item, $cart_item_key, $values, $order) {
    if (empty($values['choice-for-cart']) || empty($values['sub-choice-for-cart'])) {
        return;
    }
    
    $item->add_meta_data(__('Choice', 'iconic'), $values['choice-for-cart']);
    $item->add_meta_data(__('Sub-Choice', 'iconic'), $values['sub-choice-for-cart']);
}

add_action('woocommerce_checkout_create_order_line_item', 'iconic_add_engraving_text_to_order_items', 10, 4);

function activeStatus($slug) {
    $current_id = get_the_id();
    $page = get_page_by_path($slug);
    $page_id = $page->ID;
    if ($page_id == $current_id) {
        echo 'active';
    }
}

function pagesClasses() {
    if (is_checkout()) {
        echo "checkout-page";
    } elseif (is_cart()) {
        echo "cart-page";
    }
    
}