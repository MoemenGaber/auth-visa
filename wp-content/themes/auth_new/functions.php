<?php
/**
 * Auth_new functions and definitions
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Auth_new
 */

if (!function_exists('auth_new_setup')) :
  /**
   * Sets up theme defaults and registers support for various WordPress features.
   *
   * Note that this function is hooked into the after_setup_theme hook, which
   * runs before the init hook. The init hook is too late for some features, such
   * as indicating support for post thumbnails.
   */
  function auth_new_setup() {
    /*
 * Make theme available for translation.
 * Translations can be filed in the /languages/ directory.
 * If you're building a theme based on Auth_new, use a find and replace
 * to change 'auth_new' to the name of your theme in all the template files.
 */
    load_theme_textdomain('auth_new', get_template_directory() . '/languages');
    
    // Add default posts and comments RSS feed links to head.
    add_theme_support('automatic-feed-links');
    
    /*
 * Let WordPress manage the document title.
 * By adding theme support, we declare that this theme does not use a
 * hard-coded <title> tag in the document head, and expect WordPress to
 * provide it for us.
 */
    add_theme_support('title-tag');
    
    /*
 * Enable support for Post Thumbnails on posts and pages.
 *
 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
 */
    add_theme_support('post-thumbnails');
    
    // This theme uses wp_nav_menu() in one location.
    register_nav_menus(array('menu-1' => esc_html__('Primary', 'auth_new'),));
    
    /*
 * Switch default core markup for search form, comment form, and comments
 * to output valid HTML5.
 */
    add_theme_support('html5', array(
      'search-form',
      'comment-form',
      'comment-list',
      'gallery',
      'caption',
    ));
    
    // Set up the WordPress core custom background feature.
    add_theme_support('custom-background', apply_filters('auth_new_custom_background_args', array(
      'default-color' => 'ffffff',
      'default-image' => '',
    )));
    
    // Add theme support for selective refresh for widgets.
    add_theme_support('customize-selective-refresh-widgets');
    
    /**
     * Add support for core custom logo.
     *
     * @link https://codex.wordpress.org/Theme_Logo
     */
    add_theme_support('custom-logo', array(
      'height'      => 250,
      'width'       => 250,
      'flex-width'  => true,
      'flex-height' => true,
    ));
  }
endif;
add_action('after_setup_theme', 'auth_new_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function auth_new_content_width() {
  // This variable is intended to be overruled from themes.
  // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
  // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
  $GLOBALS['content_width'] = apply_filters('auth_new_content_width', 640);
}
add_action('after_setup_theme', 'auth_new_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function auth_new_widgets_init() {
  register_sidebar(array(
                     'name'          => esc_html__('Sidebar', 'auth_new'),
                     'id'            => 'sidebar-1',
                     'description'   => esc_html__('Add widgets here.', 'auth_new'),
                     'before_widget' => '<section id="%1$s" class="widget %2$s">',
                     'after_widget'  => '</section>',
                     'before_title'  => '<h2 class="widget-title">',
                     'after_title'   => '</h2>',
                   ));
}
add_action('widgets_init', 'auth_new_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function auth_new_scripts() {
  //	wp_enqueue_style( 'auth_new-style', get_stylesheet_uri() );
  
  wp_enqueue_script('auth_new-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true);
  
  wp_enqueue_script('auth_new-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true);
  
  if (is_singular() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }
}
add_action('wp_enqueue_scripts', 'auth_new_scripts');

/**
 * Re-calculates and updates the cart item price
 *
 * @param      $cart_item
 *
 * @param bool $is_object
 *
 * @return float|int
 */
function update_item_price($cart_item, $is_object = false) {
  if ($is_object) {
    $first_step = get_field('first_step', $cart_item->get_id());
    $qty = 1;
  } else {
    $first_step = get_field('first_step', $cart_item['product_id']);
    $qty = $cart_item['quantity'];
  }
  
    $choice_for_cart_id = $cart_item['choice_for_cart_id'];
    $sub_choice_for_cart_id = $cart_item['sub_choice_for_cart_id'];
//    echo $choice_for_cart_id;
    
    if($sub_choice_for_cart_id=='null'){
        
        $field = $first_step['choice'][$choice_for_cart_id]['choice_fees'];
    }else{
        $field = $first_step['choice'][$choice_for_cart_id]['sub_choices'][$sub_choice_for_cart_id]['sub_choice_fees'];
        
    }
    
  
  
  @$first_document_fee =  $field['first_document_fee'] / $qty;
  @$additional_document_fee = $field['additional_document_fee'] * ($qty - 1) / $qty;
  $custom_fees = 0;
  
  foreach ($field['fees'] as $fee) {
    $custom_fees += $fee['fee_price'];
  }
  
  $final_item_price = $first_document_fee + $additional_document_fee + $custom_fees;
  !$is_object ? $cart_item['data']->set_price($final_item_price) : null;
  
  return $final_item_price;
  
}


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
  require get_template_directory() . '/inc/jetpack.php';
}

require get_template_directory() . '/inc/theme-functions.php';


/**
 * Redirect shop to homepage
 */
function custom_shop_page_redirect() {
  if (is_shop()) {
    wp_redirect(home_url());
    exit();
  }
}
add_action('template_redirect', 'custom_shop_page_redirect');


/**
 * load site ajax logic
 */

$ajax_user_type = array(
  'update_cart' => 1,
  'get_prices'  => 2
);


function auth_ajax_scripts() {
  
  global $wp_query, $ajax_user_type;
  
  // register our main script but do not enqueue it yet
  wp_register_script('auth_site_ajax', get_template_directory_uri() . '/js/authAjax.js', array('jquery'));
  
  // now the most interesting part
  // we have to pass parameters to authAjax.js script but we can get the parameters values only in PHP
  // you can define variables directly in your HTML but I decided that the most proper way is wp_localize_script()
  wp_localize_script('auth_site_ajax', 'auth_user_ajax_params', array(
    // WordPress AJAX
    'ajax_url'  => site_url() . '/wp-admin/admin-ajax.php',
    'ajax_type' => $ajax_user_type
  ));
  
  wp_enqueue_script('auth_site_ajax');
}

add_action('wp_enqueue_scripts', 'auth_ajax_scripts');

/**
 * load more ajax handler
 */
function auth_user_ajax_handler() {
  
  global $ajax_user_type;
  // prepare our arguments for the query
  switch ($_POST['ajax_type']) {
    case $ajax_user_type['update_cart'] :
      
      $cart_item_key = $_POST['cart_item_key'];
      $cart_item_quantity = $_POST['cart_item_quantity'];
      
      WC()->cart->set_quantity($cart_item_key, $cart_item_quantity);
      $cart_item = WC()->cart->get_cart_item($cart_item_key);
      $data = array(
        'product' => $cart_item_quantity != 0 ? wc_price(update_item_price($cart_item) * $cart_item['quantity']) : false,
        'total'   => get_woocommerce_currency_symbol() . WC()->cart->cart_contents_total,
      );
      
      echo json_encode($data);
      break;
    case $ajax_user_type['get_prices']:
      
      
      $choice_for_cart_id = $_POST['choice_for_cart_id'];
      $sub_choice_for_cart_id = $_POST['sub_choice_for_cart_id'];
      $product_id = $_POST['product_id'];
      
      if($sub_choice_for_cart_id=='null'){
  
        $field = get_field('first_step', $product_id)['choice'][$choice_for_cart_id]['choice_fees'];
      }else{
        $field = get_field('first_step', $product_id)['choice'][$choice_for_cart_id]['sub_choices'][$sub_choice_for_cart_id]['sub_choice_fees'];
        
      }
  
      $data = array(
        'first' => $field['first_document_fee'],
        'additional' => $field['additional_document_fee'],
        'fees' => $field['fees'],
      );

      echo json_encode($data);
      break;
  }
  die();    // here we exit the script and even no wp_reset_query() required!
}


add_action('wp_ajax_authUserAjax', 'auth_user_ajax_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_authUserAjax', 'auth_user_ajax_handler'); // wp_ajax_nopriv_{action}



add_filter('woocommerce_is_purchasable', 'preorder_is_purchasable', 10, 2);

function preorder_is_purchasable( $is_purchasable, $object ) {
		return true;
}