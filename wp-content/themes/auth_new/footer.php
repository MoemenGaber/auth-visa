<!-- region global-->
<div class="globalFooter">
    <div class="container">
        <div class="footer">
            <ul class="list-unstyled footer-links">
                <li class="first-link">Main Links</li>
                <li class="link"><a href="<?php echo site_url(); ?>">Home</a></li>
                <li class="link special"><a href="<?php echo get_permalink(get_page_by_path('pay-invoice')); ?>">Pay Invoice</a></li>
            </ul>
            <ul class="list-unstyled footer-links">
                <li class="link second-link">Place Order</li>
                <li class="link"><a href="<?php echo get_permalink(get_page_by_path('contact-us')); ?>">Contact Us</a></li>
            </ul>
            <ul class="list-unstyled footer-links">
                <li class="first-link">Follow us on</li>
                <li class="link"><i class="fab fa-facebook-f"></i> <a href="<?php echo get_field('footer_facebook_link','option'); ?>">Facebook</a></li>
                <li class="link"><i class="fab fa-twitter"></i> <a href="<?php echo get_field('footer_twitter_link','option'); ?>">Twitter</a></li>
            </ul>
            <ul class="list-unstyled footer-links">
                <li class="link second-link"><i class="fab fa-instagram"></i> <a href="<?php echo get_field('footer_instagram_link','option'); ?>">Instagram</a></li>
            </ul>
            <ul class="list-unstyled footer-links">
                <li class="link"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/globalFooter/cards.png" alt=""></li>
            </ul>
        </div>
        <div class="copyRight">
            <div class="right">
                <p>© Copyright <span>Name Here 2019</span></p>
            </div>
            <div class="privacy">
                <p><span>Terms</span> Privacy</p>

            </div>
        </div>
    </div>
</div>
<!-- endregion global-->

