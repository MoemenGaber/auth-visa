<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout.
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
	return;
}

?>
<div class="loading-overlay">
  <i class="fas fa-spinner fa-pulse"></i>
</div>
<div class="wizard04">
    <div class="content">
        <div class="container">
            <div class="documents">
                <div class="document">
                    <div class="customProgress">
                        <div class="prg prg1">
                            <img class="yes" src="<?php echo get_template_directory_uri(); ?>/assets/images/wizard02-1/yes.png" alt="">
                        </div>
                        <div class="prg prg2">
                            <img class="yes" src="<?php echo get_template_directory_uri(); ?>/assets/images/wizard02-1/yes.png" alt="">
                            <div class="background" id="numTwo"></div>
                        </div>
                        <div class="prg prg3">
                            <img  class="yes" src="<?php echo get_template_directory_uri(); ?>/assets/images/wizard02-1/yes.png" alt="">
                            <div class="background" id="numThree"></div>
                        </div>
                        <div class="prg prg4">
                            <img style="z-index: 99" class="yes" src="<?php echo get_template_directory_uri(); ?>/assets/images/wizard02-1/yes.png" alt="">
                            <div style="width: 100%"  class="background" id="numFour"></div>
                        </div>

                    </div>
                    <div class="numbers">
                        <div class="number custom">
                            <h2><span class="nub">01</span> <span><span class="step">Step 1</span><span
                                            class="doc">Documents</span></span>
                            </h2>
                        </div>
                        <div class="number custom">
                            <h2 class="numTwo" data-width="#numTwo"><span class="nub">02</span> <span><span
                                            class="step">Step 2</span><span class="doc">Timeline</span></span>
                            </h2>
                        </div>
                        <div class="number custom">
                            <h2 class="numThree " data-width="#numThree"><span class="nub">03</span> <span><span
                                            class="step">Step 3</span><span
                                            class="doc order">Order Details</span></span>
                            </h2>
                        </div>
                        <div class="number custom">
                            <h2 class="numFour active" data-width="#numFour"><span class="nub">04</span> <span><span
                                            class="step">Step 4</span><span class="doc">Payment</span></span>
                            </h2>
                        </div>
                    </div>
                    <div class="step4Form">
<form name="checkout" method="post" class="pay checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<?php if ( $checkout->get_checkout_fields() ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="col3-set" id="customer_details">
            <div class="col-6">
                <div class="step4ToPay">
                    <div class="step4">
                        <div class="box box1">
                            <p>Our mailing address is</p>
                            <p class="padding">1615 Bay Head Rd. Annapolis MD 21409</p>
                            <p class="padding">(410) 349 - 4900</p>
                        </div>
                        <div class="box box2">
                            <p class="padding">This form has memory. Fill the form the first time, and from then on
                                it will fill itself after you enter your zip code and email address.</p>
                        </div>
                        <div class="box box3">
                            <p class="padding">Our form never keeps any credit card information in the memory..</p>
                        </div>
                        <div class="box box4">
                            <p class="padding">Billing is available for our established customers. It may not be
                                available for new customers, or in cases where the embassy fee is high (as in UAE
                                $600/doc).</p>
                        </div>
                        <div class="box box5">
                            <p class="padding">The reference field is reserved for customer use. It is mostly used
                                by attorneys for a file or case number, but it can be used by any company as
                                desired, or can be left blank.</p>
                        </div>
                        <div class="box box6">
                            <h3>Estimated amount to pay</h3>
                            <h2>
                                $<?php echo WC()->cart->cart_contents_total; ?>
                                
                            </h2>
                        </div>
                    </div>
            </div>
            </div>
			<div class="col-6">
				<?php do_action( 'woocommerce_checkout_billing' ); ?>

                <?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

                <div id="order_review" class="woocommerce-checkout-review-order">
                    <?php do_action( 'woocommerce_checkout_order_review' ); ?>
                </div>

                <?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
			</div>

<!--			<div class="col-4">-->
<!--				--><?php //do_action( 'woocommerce_checkout_shipping' ); ?>
<!--			</div>-->
		</div>

		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

	<?php endif; ?>
	
	<?php do_action( 'woocommerce_checkout_before_order_review_heading' ); ?>
	
<!--	<h3 id="order_review_heading">--><?php //esc_html_e( 'Your order', 'woocommerce' ); ?><!--</h3>-->
<?php WC()->shipping->get_shipping_methods(); ?>

</form>
                    </div>  </div></div></div></div></div>
<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
