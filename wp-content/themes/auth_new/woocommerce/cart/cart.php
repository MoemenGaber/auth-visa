<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

defined('ABSPATH') || exit;
global $woocommerce;
do_action('woocommerce_before_cart'); ?>

<div class="wizard-03">
  <div class="loading-overlay">
    <i class="fas fa-spinner fa-pulse"></i>
  </div>
  <div class="content">
    <div class="container">
      <div class="documents">
        <div class="document">
          <div class="customProgress">
            <div class="prg prg1">
              <img class="yes" src="<?php echo get_template_directory_uri(); ?>/assets/images/wizard02-1/yes.png" alt="">
            </div>
            <div class="prg prg2">
              <img class="yes" src="<?php echo get_template_directory_uri(); ?>/assets/images/wizard02-1/yes.png" alt="">
              <div class="background" id="numTwo"></div>
            </div>
            <div class="prg prg3">
              <img style="z-index: 99" class="yes" src="<?php echo get_template_directory_uri(); ?>/assets/images/wizard02-1/yes.png" alt="">
              <div style="width: 100%" class="background" id="numThree"></div>
            </div>
            <div class="prg prg4">
              <img class="yes" src="<?php echo get_template_directory_uri(); ?>/assets/images/wizard02-1/yes.png" alt="">
              <div class="background" id="numFour"></div>
            </div>
          
          </div>
          <div class="numbers">
            <div class="number custom">
              <h2><span class="nub">01</span> <span><span class="step">Step 1</span><span
                    class="doc">Documents</span></span>
              </h2>
            </div>
            <div class="number custom">
              <h2 class="numTwo" data-width="#numTwo"><span class="nub">02</span> <span><span
                    class="step">Step 2</span><span class="doc">Timeline</span></span>
              </h2>
            </div>
            <div class="number custom">
              <h2 class="numThree active" data-width="#numThree"><span class="nub">03</span> <span><span
                    class="step">Step 3</span><span
                    class="doc order">Order Details</span></span>
              </h2>
            </div>
            <div class="number custom">
              <h2 class="numFour" data-width="#numFour"><span class="nub">04</span> <span><span
                    class="step">Step 4</span><span class="doc">Payment</span></span>
              </h2>
            </div>
          </div>
          <form class="woocommerce-cart-form" action="<?php echo esc_url(wc_get_cart_url()); ?>" method="post">
						<?php do_action('woocommerce_before_cart_table'); ?>
            
            <div class="deleteAll cart-nav-wrapper">
                <span class="col-md-2">Country</span>
                <span class="col-md-1">Count</span>
                <span class="col-md-2">Fees</span>
                <span class="col-md-2" style="font-size: 17px">Recieving Date</span>
                <span class="col-md-2"><img
                    src="images/wizard-03/delete.png" alt="">Delete All</span>
            
            </div>
						<?php
						foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
							$_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
							$product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);
							if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
								$product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key);
								?>
                <div class="deleteAll deleteAll2 woocommerce-cart-form__cart-item <?php echo esc_attr(apply_filters('woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key)); ?>">
                    <div class="col-md-2 table-item-wrapper">
                      <div class="d-md-none">Country:</div>  <?php
                      if (!$product_permalink) {
                        echo wp_kses_post(apply_filters('woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key).'&nbsp;');
                      } else {
                        echo wp_kses_post(apply_filters('woocommerce_cart_item_name', sprintf('<a href="%s">%s</a>', esc_url($product_permalink), $_product->get_name()), $cart_item, $cart_item_key));
                      }
          
                      do_action('woocommerce_after_cart_item_name', $cart_item, $cart_item_key);
          
                      // Meta data.
                      echo wc_get_formatted_cart_item_data($cart_item); // PHPCS: XSS ok.
          
                      // Backorder notification.
                      if ($_product->backorders_require_notification() && $_product->is_on_backorder($cart_item['quantity'])) {
                        echo wp_kses_post(apply_filters('woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">'.esc_html__('Available on backorder', 'woocommerce').'</p>', $product_id));
                      }
                      ?>
                    </div>
                    <span class="col-md-1 table-item-wrapper">
                                      <div class="d-md-none">Quantity:</div>
                                 <select name="cart[<?=$cart_item_key?>][qty]" class="" data-cart-item-key="<?=$cart_item_key?>">
                                     <?php
                                     for ($i = 1; $i <= 7; $i++) { ?>
                                       <option value="<?=$i?>" <?=$cart_item['quantity'] == $i ? 'selected' : ''?> ><?=$i?></option><?php
                                     } ?>
                                 </select>
                                  
                                    </span>
                    <span class="col-md-2 price table-item-wrapper">
                                      <div class="d-md-none">Price:</div>
                      
                <?php echo wc_price(update_item_price($cart_item) * $cart_item['quantity']); ?>

            </span>
                    <span class="col-md-2 table-item-wrapper"> <div class="d-md-none">Date:</div> 9/25/2019</span>
                    <a href="<?php echo wc_get_cart_remove_url($cart_item_key); ?>">
                                          <span class="col-md-2"><img
                                              src="<?php echo get_template_directory_uri(); ?>/assets/images/wizard-03/delete.png" alt="">Delete All</span></a>
                    <span class="type" style="font-size: 10px"> <?php echo ($cart_item['choice_for_cart'])?$cart_item['choice_for_cart']:''; ?></span>
                  <span class="sub-type" style="font-size: 10px"> <?php echo ($cart_item['sub_choice_for_cart'] !='null')?$cart_item['sub_choice_for_cart']:''; ?></span>


                </div>
							<?php }
						} ?>
						<?php
						?>
            
            <!--                        TODO mody update cart button-->
            <button type="submit" class="d-none button" name="update_cart" value="<?php esc_attr_e('Update cart', 'woocommerce'); ?>"><?php esc_html_e('Update cart', 'woocommerce'); ?></button>
						
						
						<?php do_action('woocommerce_cart_actions'); ?>
						
						<?php wp_nonce_field('woocommerce-cart', 'woocommerce-cart-nonce'); ?>
            
            <div class="nextPrev">
              <div class="amount">
                <h3>tOTAL aMOUNT</h3>
                <h2 id="total-amount">
									<?php echo get_woocommerce_currency_symbol().WC()->cart->cart_contents_total; ?>
                </h2>
              </div>
              <a class="cart-next-to-checkout" href="<?php echo wc_get_checkout_url(); ?>">
                Next
              </a>
            </div>
						<?php do_action('woocommerce_after_cart_table'); ?>
          
          </form>
          <button class="button add-another-doc-cart"><a href="<?php echo get_home_url(); ?>">Add another document</a></button>
          <!--                    <button type="submit" class="button" name="update_cart" value="--><?php //esc_attr_e( 'Update cart', 'woocommerce' ); ?><!--">-->
					<?php //esc_html_e( 'Update cart', 'woocommerce' ); ?><!--</button>-->
					
					<?php do_action('woocommerce_cart_actions'); ?>
					
					<?php wp_nonce_field('woocommerce-cart', 'woocommerce-cart-nonce'); ?>
					<?php do_action('woocommerce_before_cart_collaterals'); ?>
          
          <div class="cart-collaterals">
						<?php
						/**
						 * Cart collaterals hook.
						 *
						 * @hooked woocommerce_cross_sell_display
						 * @hooked woocommerce_cart_totals - 10
						 */
						do_action('woocommerce_cart_collaterals');
						?>
          </div>
					
					<?php do_action('woocommerce_after_cart'); ?>
